package core.configuration;

public class configProperties {
    private static TestsConfig appConfig = new TestsConfig();

    public String Device = appConfig.getDevice();

    public String UserName = appConfig.getUserName();
    public String Password = appConfig.getPassword();

    public String Environment = appConfig.getEnvironment();
    public String isEnableReporting = appConfig.getIsEnableReporting();
    
//##################### --- Settings Respective to Environments --- #####################

    //                      ############ --- QA --- ############
    public String QA_baseUri = appConfig.getQA_baseUri();
    //                                 # --- DB --- #
    public String QA_dbUrl = appConfig.getQA_dbUrl();
    public String QA_dbUserName = appConfig.getQA_dbUserName();
    public String QA_dbPassword = appConfig.getQA_dbPassword();

}
