package core.configuration;

import core.configuration.testProperties.PropertiesLoader;
import core.configuration.testProperties.Property;
import core.configuration.testProperties.PropertyFile;

@PropertyFile("config.properties")
public class TestsConfig {

    private static TestsConfig config;

    public static TestsConfig getConfig() {
        if (config == null) {
            config = new TestsConfig();
        }
        return config;
    }

    TestsConfig() {
        PropertiesLoader.populate(this);
    }

    @Property("Device")
    private String Device ="";
    @Property("project")
    private String project = "";
    @Property("projectKey")
    private String projectKey = "";
    @Property("cycleName")
    private String cycleName = "";
    @Property("releaseVersion")
    private String releaseVersion = "";

    @Property("userName")
    private String userName = "";
    @Property("password")
    private String password = "";

    @Property("environment")
    private String environment = "";
    @Property("isEnableReporting")
    private String isEnableReporting = "";

    //##################### --- Settings Respective to Environments --- #####################
    //                      ############ --- QA --- ############
    @Property("QA_keyCloakUri")
    private String QA_keyCloakUri = "";
    @Property("QA_baseUri")
    private String QA_baseUri = "";
    @Property("QA_port")
    private String QA_port = "";

    //                                 # --- DB --- #
    @Property("QA_dbUrl")
    private String QA_dbUrl = "";
    @Property("QA_dbUserName")
    private String QA_dbUserName = "";
    @Property("QA_dbPassword")
    private String QA_dbPassword = "";


    String getDevice() {return Device;}
    public String getProject() {
        return project;
    }
    public String getProjectKey() {
        return projectKey;
    }
    public String getCycleName() {
        return cycleName;
    }
    public String getReleaseVersion() {
        return releaseVersion;
    }

    String getUserName() {return userName;}
    String getPassword() {return password;}

    String getEnvironment() {return environment;}
    String getIsEnableReporting() {return isEnableReporting;}


//##################### --- Settings Respective to Environments --- #####################
//                      ############ --- QA --- ############

    String getQA_baseUri() {
        return QA_baseUri;
    }

    //                                 # --- DB --- #
    String getQA_dbUrl() { return QA_dbUrl; }
    String getQA_dbUserName() { return QA_dbUserName; }
    String getQA_dbPassword() { return QA_dbPassword; }

}
