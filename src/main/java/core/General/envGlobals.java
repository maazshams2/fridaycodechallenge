package core.General;

import core.configuration.configProperties;
import org.json.JSONArray;

import java.util.*;

// Global variables used in project
public class envGlobals{

    private static configProperties configProps = new configProperties();
    public static Date time = Generic.getTime();
    public static List<String> simpleList = new ArrayList<>();
    public static List<String> simpleAssert = new ArrayList<>();
    public static List<String> complicatedList = new ArrayList<>();
    public static List<String> complicatedAssert = new ArrayList<>();
    public static List<String> countryList = new ArrayList<>();
    public static List<String> countryAssert = new ArrayList<>();
    public static List<JSONArray> jsonArrayList = new ArrayList<>();

    public static String street = "";
    public static String houseNo = "";
    public static String streetKey = "street";
    public static String houseNoKey = "housenumber";
    public static boolean check = true;
}
