package module;

import core.General.Generic;
import org.testng.Assert;
import org.testng.annotations.Test;
import stepDef.jsonTest;

import static core.General.Generic.*;
import static core.General.envGlobals.*;

public class Tests {
    public Tests(){}

    // Test execution
    @Test
    public void simple() {
        System.out.println("\nSIMPLE");

        simpleList.add("Winterallee 3");
        simpleAssert.add("{\"" + streetKey + "\":\"Winterallee\",\"" + houseNoKey + "\":\"3\"}");
        simpleList.add("Musterstrasse 45");
        simpleAssert.add("{\"" + streetKey + "\":\"Musterstrasse\",\"" + houseNoKey + "\":\"45\"}");
        simpleList.add("Blaufeldweg 123B");
        simpleAssert.add("{\"" + streetKey + "\":\"Blaufeldweg\",\"" + houseNoKey + "\":\"123B\"}");

        for (int i=0 ; i<simpleList.size() ; i++) {
            String json = jsonTest.convert(simpleList.get(i));
            Assert.assertEquals(json, simpleAssert.get(i));
        }
    }

    @Test
    public void complicated() {
        System.out.println("\nCOMPLICATED");

        complicatedList.add("Am Bächle 23");
        complicatedAssert.add("{\"" + streetKey + "\":\"Am Bächle\",\"" + houseNoKey + "\":\"23\"}");
        complicatedList.add("Auf der Vogelwiese 23 b");
        complicatedAssert.add("{\"" + streetKey + "\":\"Auf der Vogelwiese\",\"" + houseNoKey + "\":\"23 b\"}");

        for (int i=0 ; i<complicatedList.size() ; i++) {
            String json = jsonTest.convert(complicatedList.get(i));
            Assert.assertEquals(json, complicatedAssert.get(i));
        }
    }

    @Test
    public void countries() {
        System.out.println("\nCOUNTRIES");

        countryList.add("4, rue de la revolution");
        countryAssert.add("{\"" + streetKey + "\":\"rue de la revolution\",\"" + houseNoKey + "\":\"4\"}");
        countryList.add("200 Broadway Av");
        countryAssert.add("{\"" + streetKey + "\":\"Broadway Av\",\"" + houseNoKey + "\":\"200\"}");
        countryList.add("Calle Aduana, 29");
        countryAssert.add("{\"" + streetKey + "\":\"Calle Aduana\",\"" + houseNoKey + "\":\"29\"}");
        countryList.add("Calle 39 No 1540");
        countryAssert.add("{\"" + streetKey + "\":\"Calle 39\",\"" + houseNoKey + "\":\"No 1540\"}");

        for (int i=0 ; i<countryList.size() ; i++) {
            String json = jsonTest.convert(countryList.get(i));
            Assert.assertEquals(json, countryAssert.get(i));
        }
    }
}
