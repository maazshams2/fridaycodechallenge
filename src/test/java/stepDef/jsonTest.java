package stepDef;

import com.google.gson.JsonObject;
import core.General.Generic;

import java.util.regex.Pattern;

import static core.General.envGlobals.*;

public class jsonTest {
    public jsonTest(){}

    public static String createJSON(String street, String houseNumber){
        // Create new JSON Object
        JsonObject addressJson = new JsonObject();

        addressJson.addProperty("street", street);
        addressJson.addProperty("housenumber", houseNumber);

        return addressJson.toString();
    }

    public static void breakAddress(String[] arr){
        Pattern numeric = Pattern.compile( "[0-9]" );
        StringBuilder streetBuilder = new StringBuilder();
        StringBuilder houseNoBuilder = new StringBuilder();

        for (int i=0 ; i<arr.length ; i++) {
            if (!numeric.matcher(arr[i]).find()) {
                if (arr[i].equalsIgnoreCase("no") || arr[i].length()<2)
                    houseNoBuilder.append(arr[i]).append(" ");
                else
                    streetBuilder.append(arr[i]).append(" ");
            }
            else {
                if (i<arr.length-1 && arr[i+1].equalsIgnoreCase("no"))
                    streetBuilder.append(arr[i]).append(" ");
                else
                    houseNoBuilder.append(arr[i]).append(" ");
            }
        }

        street = streetBuilder.toString().substring(0, streetBuilder.length()-1);
        houseNo = houseNoBuilder.toString().substring(0, houseNoBuilder.length()-1);
        check = true;
    }

    public static String convert(String str){
        jsonTest.breakAddress(Generic.toArray(str));

        String jsonString = jsonTest.createJSON(street, houseNo);
        System.out.println(jsonString);

        return jsonString;
    }
}
